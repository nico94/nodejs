var socket = io.connect('http://192.168.50.100:8081', { 'forceNew': true });

socket.on('cedula', function(data){
    render(data);
});

function render(data) {
    var parsed = JSON.parse(data);
    var arr1 = [];
    for (var x in parsed) {
        arr1.push(parsed[x]);
    }
    console.log(arr1);
    var html = arr1.map(function (elem, index) {
        return (`<div>
        		 <strong>${elem}</strong>
        </div>
        
        `)
    }).join(" ");
    document.getElementById('mensaje').innerHTML = html;
}

function enviarNotificacion(e){
    var payLoad = {
        mensaje: document.getElementById('mensaje').value
    };
    
    console.log('enviar notificacion '+document.getElementById('mensaje').value);
    socket.emit('notificacion-new', payLoad);
    return false;
}