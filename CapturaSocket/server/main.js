var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);

app.use(express.static('public'));

io.on('connection', function (socket) {
    console.log('Un cliente se ha conectado');

    socket.on('disconnect', function () {
        console.log('Usuario desconectado');
    });

    socket.on('cedula-new',(data) =>{
        console.log("Evento cedula");
        io.emit('cedula',data);
    });

    socket.on('notificacion-new',(data)=>{
        console.log('evento de notificacion');
        io.emit('notificacion',data);
    });
});

server.listen(8081, function () {
    console.log('Servidor corriendo en http://localhost:8081');
});
